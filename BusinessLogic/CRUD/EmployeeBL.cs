﻿using DataAccess;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.CRUD
{
    //Here we find all logic related to crud of entity
    public class EmployeeBL
    {
        private readonly EmployeeDA employeeDA = new EmployeeDA();

        //filter list of employee and add logic of business, for example not get all deleted. ruler of business.
        public List<Employee> FindAll(string EmployeeLastName = null, string EmployeePhone = null)
        {
            ///Validation to remove invalid mask character
            if (EmployeePhone != null)
            {
                EmployeePhone = EmployeePhone.Replace("_", "").Trim();

                if (EmployeePhone.Length <= 10)
                    EmployeePhone = EmployeePhone.Replace("-", "").Trim();

                if (EmployeePhone.Length <= 4)
                    EmployeePhone = EmployeePhone.Replace(")", "").Trim();
            }


            return employeeDA.GetAll(new Employee() { DeletedFilter = false, EmployeeLastName = EmployeeLastName ?? "", EmployeePhone = EmployeePhone ?? "" })
            .OrderBy(a => a.HireDate).ToList();
        }

        /// <summary>
        /// Find Employee with unique Id
        /// </summary>
        /// <param name="id">Unique Id GUID that represent one employee</param>
        /// <returns></returns>
        public Employee FindById(Guid id)
        {
            return employeeDA.GetById(id);
        }

        /// <summary>
        /// Insert a new employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public CRUDResponse Insert(Employee employee)
        {
            if (employee.EmployeeID == Guid.Empty)
                employee.EmployeeID = Guid.NewGuid();
            var response = Valid(employee);

            if (!response.Success)
                return response;

            employee.Deleted = false;
            employeeDA.Insert(employee);

            response.Message = "Created";
            return response;
        }

        /// <summary>
        /// Update existing employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public CRUDResponse Update(Employee employee)
        {
            var response = Valid(employee);
            if (!response.Success)
                return response;

            employee.Deleted = false;
            employeeDA.Update(employee);

            response.Message = "Updated";
            return response;
        }

        /// <summary>
        /// Delete existing employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CRUDResponse Delete(Guid id)
        {
            if (id == Guid.Empty)
                return new CRUDResponse(false, "id is requerid");
            var obj = employeeDA.GetById(id);

            if (obj == null)
                return new CRUDResponse(false, "Employee not found");

            obj.Deleted = true;
            employeeDA.Update(obj);
            return new CRUDResponse(true, "Deleted");
        }

        /// <summary>
        /// Check if all required fields are filled
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        private CRUDResponse Valid(Employee employee)
        {

            if (employee.EmployeeID == null || employee.EmployeeID == Guid.Empty)
                return new CRUDResponse(false, "EmployeeID is requerid");

            if (string.IsNullOrEmpty(employee.EmployeeZip))
                return new CRUDResponse(false, "EmployeeZip is requerid");

            if (string.IsNullOrEmpty(employee.EmployeePhone))
                return new CRUDResponse(false, "EmployeePhone is requerid");

            if (string.IsNullOrEmpty(employee.EmployeeFirstName))
                return new CRUDResponse(false, "EmployeeFirstName is requerid");

            if (string.IsNullOrEmpty(employee.EmployeeLastName))
                return new CRUDResponse(false, "EmployeeLastName is requerid");


            return new CRUDResponse(true);

        }
    }
}