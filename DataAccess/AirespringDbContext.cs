﻿using DataAccess.Models;
using DataAccess.Seed;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    internal class AirespringDbContext : DbContext
    {

        public AirespringDbContext() : base("AirespringDbContext")
        {
            
            Database.SetInitializer<AirespringDbContext>(new EmployeeInitializer());

        }

        public DbSet<Employee> Employees { get; set; }

    }
}
