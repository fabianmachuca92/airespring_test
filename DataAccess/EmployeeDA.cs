﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class EmployeeDA
    {
        /// <summary>
        /// Generic method to filter table employee
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<Employee> GetAll(Employee filter = null)
        {
            using (AirespringDbContext dbContext = new AirespringDbContext())
            {
                IQueryable<Employee> query = dbContext.Employees;
                if (filter != null)
                {
                    if (!string.IsNullOrEmpty(filter.EmployeeLastName))
                        query = query.Where(a => a.EmployeeLastName.Contains(filter.EmployeeLastName));

                    if (!string.IsNullOrEmpty(filter.EmployeePhone))
                        query = query.Where(a => a.EmployeePhone.Contains(filter.EmployeePhone));

                    if (filter.DeletedFilter.HasValue)
                        query = query.Where(a => a.Deleted == filter.DeletedFilter);


                }


                return query.AsNoTracking().ToList();
            }
        }

        /// <summary>
        /// Find one employee by unique id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Employee GetById(Guid id)
        {
            using (AirespringDbContext dbContext = new AirespringDbContext())
                return dbContext.Employees.Find(id);
        }

        /// <summary>
        /// Insert a employee
        /// </summary>
        /// <param name="newObj"></param>
        public void Insert(Employee newObj)
        {
            using (AirespringDbContext dbContext = new AirespringDbContext())
            {
                dbContext.Employees.Add(newObj);
                dbContext.SaveChanges();
            }
        }

        /// <summary>
        /// Update a employee
        /// </summary>
        /// <param name="updateObj"></param>
        public void Update(Employee updateObj)
        {
            using (AirespringDbContext dbContext = new AirespringDbContext())
            {
                dbContext.Entry(updateObj).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
        }

    }
}
