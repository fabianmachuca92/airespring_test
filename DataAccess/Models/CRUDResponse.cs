﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class CRUDResponse
    {
        public CRUDResponse(bool success, string message = null)
        {
            Success = success;
            Message = message ?? string.Empty;
        }

        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
