﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class Employee
    {
        [Key]
        public Guid EmployeeID { get; set; }

        [Required, StringLength(200)]
        public string EmployeeLastName { get; set; }
        
        [Required, StringLength(200)]
        public string EmployeeFirstName { get; set; }
        
        [Required, StringLength(20)]
        public string EmployeePhone { get; set; }
        
        [Required, StringLength(10)]
        public string EmployeeZip { get; set; }
        
        [Required, DataType(DataType.Date)]
        public DateTime HireDate { get; set; }

        [Required]
        public bool Deleted { get; set; }

        [NotMapped]
        public bool? DeletedFilter { get; set; }


    }
}
