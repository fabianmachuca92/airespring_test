﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Seed
{
    internal class EmployeeInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<AirespringDbContext>
    {
        protected override void Seed(AirespringDbContext context)
        {

            var employees = new List<Employee>
            {
                new Employee{ EmployeeID=Guid.NewGuid(), EmployeeZip="FL-55547", EmployeePhone="(554)-548-1597", Deleted=false, EmployeeFirstName="Carson",EmployeeLastName="Alexander",HireDate=new DateTime(2005,05,1)},
                new Employee{EmployeeID=Guid.NewGuid(), EmployeeZip="FL-33128", EmployeePhone="(587)-428-7531", Deleted=false, EmployeeFirstName="Meredith",EmployeeLastName="Alonso",HireDate=new DateTime(2002,09,01)},
                new Employee{EmployeeID=Guid.NewGuid(), EmployeeZip="DC-20001", EmployeePhone="(685)-654-3574", Deleted=false, EmployeeFirstName="Arturo",EmployeeLastName="Anand",HireDate=new DateTime(2003,09,01)},
                new Employee{EmployeeID=Guid.NewGuid(), EmployeeZip="CA-08811", EmployeePhone="(252)-258-9512", Deleted=false, EmployeeFirstName="Gytis",EmployeeLastName="Barzdukas",HireDate=new DateTime(2002,09,01)},
                new Employee{EmployeeID=Guid.NewGuid(), EmployeeZip="NY-10010", EmployeePhone="(364)-896-7496", Deleted=false, EmployeeFirstName="Yan",EmployeeLastName="Li",HireDate=new DateTime(2002,09,01)},
                new Employee{EmployeeID=Guid.NewGuid(), EmployeeZip="DC-20233", EmployeePhone="(335)-874-2536", Deleted=false, EmployeeFirstName="Peggy",EmployeeLastName="Justice",HireDate=new DateTime(2001,09,01)}
            };
            employees.ForEach(s => context.Employees.Add(s));
            context.SaveChanges();
        }
    }
}
