﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Employee.aspx.cs" Inherits="WebForm.CRUD.Employee" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="head">
    <link href="/Assets/plugins/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div aria-live="polite" aria-atomic="true" class="d-flex position-absolute justify-content-center align-items-center ">
        <div class="toast align-items-center text-white bg-primary border-0 " style="z-index: 9999" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
        </div>
    </div>
    <h1>Employee CRUD</h1>
    <div class="row mb-2">
        <div class="col-12">
            <button type="button" id="btn_newEmployee" class="btn btn-success float-end" data-bs-toggle="modal" data-bs-target="#EmployeeModal">Add Employee</button>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <p>Advanced filters</p>
        </div>
        <div class="card-body>">
            <div class="p-3">

                <div class="col-12">
                    <div class="row">
                        <div class="col-auto">
                            <label>Last Name</label>
                            <asp:TextBox runat="server" ID="txt_lastName" MaxLength="200" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-auto">
                            <label>Phone</label>
                            <asp:TextBox runat="server" ID="txt_phone" MaxLength="20" CssClass="form-control mask-phone"></asp:TextBox>
                        </div>
                        <div class="col-auto">
                            <br />
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button ID="btn_search" CssClass="btn btn-primary" runat="server" Text="Search" OnClick="btn_search_Click" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btn_search" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>


            </div>
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="table-responsive">
                        <asp:GridView runat="server" ID="grv_Empoyee" CssClass="table hoover" ShowHeader="true"
                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-CssClass="text-center" EmptyDataText="Not empoyee found"
                            AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField DataField="EmployeeFirstName" HeaderText="First Name" />
                                <asp:BoundField DataField="EmployeeLastName" HeaderText="Last Name" />
                                <asp:BoundField DataField="EmployeePhone" HeaderText="Phone" />
                                <asp:BoundField DataField="EmployeeZip" HeaderText="Zip" />
                                <asp:BoundField DataField="HireDate" HeaderText="Hire Date" DataFormatString="{0:MM/dd/yyyy}" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button runat="server" class="btn btn-danger btn-sm" Text="Delete" CommandArgument='<%# Eval("EmployeeID") %>' OnClick="TrashEmployee_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grv_Empoyee" />
                    <asp:AsyncPostBackTrigger ControlID="btn_saveModal" />
                    <asp:AsyncPostBackTrigger ControlID="btn_search" />
                </Triggers>
            </asp:UpdatePanel>

        </div>
    </div>


    <!-- Modal Employee-->
    <div class="modal fade" id="EmployeeModal" aria-labelledby="EmployeeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Employee</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <asp:HiddenField ClientIDMode="Static" runat="server" ID="hdf_modal_id" />
                                <div class="col-12">
                                    <label>First Name</label>
                                    <input runat="server" clientidmode="Static" class="form-control" maxlength="200" autocomplete="off" id="txt_modal_firstName" />
                                </div>

                                <div class="col-12">
                                    <label>Last Name</label>
                                    <input clientidmode="Static" runat="server" class="form-control" maxlength="200" autocomplete="off" id="txt_modal_lastName" />
                                </div>
                                <div class="col-12">
                                    <label>Phone</label>
                                    <input clientidmode="Static" runat="server" class="form-control mask-phone" maxlength="20" autocomplete="off" id="txt_modal_Phone" />
                                </div>

                                <div class="col-12">
                                    <label>Zip</label>
                                    <input clientidmode="Static" runat="server" class="form-control" maxlength="10" autocomplete="off" id="txt_modal_zip" />
                                </div>

                                <div class="col-12">
                                    <label>Hire Date</label>
                                    <input clientidmode="Static" runat="server" class="form-control date" maxlength="10" autocomplete="off" id="txt_modal_hireDate" readonly />
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btn_saveModal" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" onclick="Validate()">Save</button>
                            <asp:Button ClientIDMode="Static" runat="server" CssClass="d-none" ID="btn_saveModal" OnClick="btn_saveModal_Click" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btn_saveModal" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="footer">

    <%--plugins--%>
    <script src="/Assets/plugins/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="/Assets/plugins/RobinHerbots-Inputmask/dist/jquery.inputmask.min.js"></script>
    <script src="/Assets/plugins/RobinHerbots-Inputmask/dist/inputmask.min.js"></script>

    <%--for a quick review of javascript code do not separate this section of code into a file--%>
    <script type="text/javascript">

        $(document).ready(function () {
            initEmpoyee();
        });

        ///inicialize the mask and event to open modal
        function initEmpoyee() {
            $('.mask-phone').inputmask("(999) 999-9999");
            $('.date').datepicker({ autoclose: true, endDate: new Date() });
            $('#EmployeeModal').on('show.bs.modal', function () {
                $("html, body").animate({ scrollTop: 0 });
            });
            $('#btn_newEmployee').on('click', function () { ClearModal(); });

        }

        ///function to clear fieds of modal
        function ClearModal() {

            $('#txt_modal_firstName').val('');
            $('#txt_modal_lastName').val('');
            $('#txt_modal_Phone').val('');
            $('#txt_modal_zip').val('');
            $('#txt_modal_hireDate').val('');
            $('#hdf_modal_id').val('');
        }

        //function to validate fieds before to send request to server
        function Validate() {

            if ($('#txt_modal_firstName').val() == '') {
                $('.toast-body').html('First Name is required');
                $('.toast').toast('show');
                return
            }

            if ($('#txt_modal_lastName').val() == '') {
                $('.toast-body').html('Last Name is required');
                $('.toast').toast('show');
                return
            }


            if ($('#txt_modal_Phone').val() == '') {
                $('.toast-body').html('Phone is required');
                $('.toast').toast('show');
                return
            }


            if ($('#txt_modal_zip').val() == '') {
                $('.toast-body').html('code zip is required');
                $('.toast').toast('show');
                return
            }


            if ($('#txt_modal_hireDate').val() == '') {
                $('.toast-body').html('Hire date is required');
                $('.toast').toast('show');
                return
            }

            $('#btn_saveModal').click();

        }

        //function to show success message and close modal
        function successfully() {
            $('#EmployeeModal').modal('hide');

            $('.toast-body').html('employee recorded successfully');
            $('.toast').toast('show');
        }

    </script>

</asp:Content>
