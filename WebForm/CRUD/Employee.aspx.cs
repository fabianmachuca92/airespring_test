﻿using BusinessLogic.CRUD;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm.CRUD
{
    public partial class Employee : System.Web.UI.Page
    {
        private readonly EmployeeBL employeeBL = new EmployeeBL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FetchData();
            }
        }

        /// <summary>
        /// Find to employee 
        /// </summary>
        /// <param name="lastname"></param>
        /// <param name="phone"></param>
        private void FetchData(string lastname = null, string phone = null)
        {
            grv_Empoyee.DataSource = employeeBL.FindAll(EmployeeLastName: lastname, EmployeePhone: phone);
            grv_Empoyee.DataBind();
        }

        protected void btn_saveModal_Click(object sender, EventArgs e)
        {
            try
            {
                employeeBL.Insert(new DataAccess.Models.Employee()
                {
                    EmployeeFirstName = txt_modal_firstName.Value,
                    EmployeeLastName = txt_modal_lastName.Value,
                    EmployeePhone = txt_modal_Phone.Value,
                    EmployeeZip = txt_modal_zip.Value,
                    HireDate = DateTime.ParseExact(txt_modal_hireDate.Value, "MM/dd/yyyy", CultureInfo.InvariantCulture)

                });
                FetchData();
                ScriptManager.RegisterStartupScript(this, this.Page.GetType(), Guid.NewGuid().ToString("N"), "successfully();", true);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Event to search by filters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
                FetchData(txt_lastName.Text, txt_phone.Text);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Event to delete employee
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TrashEmployee_Click(object sender, EventArgs e)
        {
            try
            {
                Guid id = Guid.Parse(((Button)sender).CommandArgument);
                employeeBL.Delete(id);
                FetchData(txt_lastName.Text, txt_phone.Text);
            }
            catch (Exception ex)
            {

            }
        }
    }
}