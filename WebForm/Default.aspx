﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebForm._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Test project</h1>
        <p class="lead">This is a test project by Gerardo Fabián Machuca Morán (+593) 989 548 582. </p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Tools</h2>
            <p>
                This project is developed with:
            </p>
            <ul>
                <li>Net Framework 4.8</li>
                <li>WebForm</li>
                <li>Entity Framework</li>
                <li>Bootstrap 5</li>
                <li>JQuery</li>
            </ul>
        </div>
       
    </div>

</asp:Content>
